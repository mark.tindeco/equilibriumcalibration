import pandas as pd
import numpy as np

cov_log_returns_usd = pd.read_pickle('cov_log_returns_usd.pkl')
mean_log_returns_usd = pd.read_pickle('mean_log_returns_usd.pkl')
weights = pd.read_pickle('weights.pkl')

time_steps = 266
representative_investor_risk_aversion = 2.1335579271755662

def portfolio_expected_utility_analytic(weights, risk_aversion):
    one_minus_risk_aversion = 1.0 - risk_aversion
    return np.exp(one_minus_risk_aversion * time_steps * (weights @ mean_log_returns_usd - 0.5 * risk_aversion * weights @ cov_log_returns_usd @ weights)) / (1.0 - risk_aversion)

def jacobean_analytic(weights, risk_aversion):
    one_minus_risk_aversion = 1.0 - risk_aversion
    return one_minus_risk_aversion * time_steps * (mean_log_returns_usd - risk_aversion * cov_log_returns_usd @ weights) * portfolio_expected_utility_analytic(weights, risk_aversion)

# The value below is the analytic version of the expected utility over the time horizon
# For the market portfolio with the risk aversion of the representative investor
portfolio_expected_utility_analytic_val = portfolio_expected_utility_analytic(weights, representative_investor_risk_aversion)

jacobean_analytic_val = jacobean_analytic(weights, representative_investor_risk_aversion)
